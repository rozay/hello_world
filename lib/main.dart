import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hello World',
      home: Scaffold(
        backgroundColor: Colors.green[100],
        body: Center(
          child: Text("Hello World"),
        ),
      ),
    );
  }
}
